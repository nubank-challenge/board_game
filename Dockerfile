FROM elixir:1.9.1-alpine

EXPOSE 4000

ARG SECRET_KEY_BASE
ENV SECRET_KEY_BASE $SECRET_KEY_BASE

ADD . /opt/app
WORKDIR /opt/app

RUN apk add tzdata

RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix deps.get --only prod
RUN MIX_ENV=prod mix compile --force

CMD MIX_ENV=prod mix compile --force && MIX_ENV=prod mix phx.server