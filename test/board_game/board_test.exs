defmodule BoardGame.BoardTest do
  use ExUnit.Case, async: true

  alias BoardGame.{Board, Robot, Dinosaur, Coordinate}

  setup do
    {:ok, board} =Board.new()
    {:ok, board: board}
  end

  test "BoardGame.Board.new/0" do
    assert Board.new() == {:ok, %Board{robots: [], dinosaurs: []}}
  end

  describe "BoardGame.Board.position_dinosaur/3" do
    test "position dinosaur with valid coordinate", %{board: board} do
      dinosaur = %Dinosaur{coordinate: %Coordinate{row: 1, col: 1}, state: :alive}
      assert Board.position_dinosaur(board, 1, 1) == {:ok, dinosaur, %{board | dinosaurs: [dinosaur]}}
    end

    test "position dinosaur with invalid coordinate", %{board: board} do
      assert Board.position_dinosaur(board, 0, 0) == {:error, :invalid_coordinate}
    end

    test "position dinosaur in busy coordinate", %{board: board} do
      dinosaur = %Dinosaur{coordinate: %Coordinate{row: 1, col: 1}, state: :alive}
      board = %{board | dinosaurs: [dinosaur]}
      assert Board.position_dinosaur(board, 1, 1) == {:error, :overlapped_coordinate}
    end
  end

  describe "BoardGame.Board.position_robot/4" do
    test "position robot with valid coordinate and face_direction", %{board: board} do
      robot = %Robot{coordinate: %Coordinate{row: 1, col: 1}, face_direction: :right}
      assert Board.position_robot(board, 1, 1, :right) == {:ok, robot, %{board | robots: [robot]}}
    end

    test "position robot with invalid coordinate", %{board: board} do
      assert Board.position_robot(board, 0, 0, :right) == {:error, :invalid_coordinate}
    end

    test "position robot with invalid face_direction", %{board: board} do
      assert Board.position_robot(board, 1, 1, :wrong) == {:error, :invalid_face_direction}
    end

    test "position robot in busy coordinate", %{board: board} do
      robot = %Robot{coordinate: %Coordinate{row: 1, col: 1}, face_direction: :right}
      board = %{board | robots: [robot]}
      assert Board.position_robot(board, 1, 1, :right) == {:error, :overlapped_coordinate}
    end
  end

  describe "BoardGame.Board.move_robot/4" do
    test "move robot with valid coordinate and movement", %{board: board} do
      board = %{board | robots: [%Robot{coordinate: %Coordinate{row: 1, col: 1}, face_direction: :right}]}
      robot = %Robot{coordinate: %Coordinate{row: 1, col: 2}, face_direction: :right}
      assert Board.move_robot(board, 1, 1, :forward) == {:ok, robot, %{board | robots: [robot]}}
    end

    test "move robot with invalid coordinate", %{board: board} do
      assert Board.move_robot(board, 0, 0, :forward) == {:error, :invalid_coordinate}
    end

    test "move robot with invalid movement", %{board: board} do
      board = %{board | robots: [%Robot{coordinate: %Coordinate{row: 1, col: 1}, face_direction: :right}]}
      assert Board.move_robot(board, 1, 1, :invalid) == {:error, :invalid_movement}
    end

    test "move robot with not found robot", %{board: board} do
      assert Board.move_robot(board, 1, 1, :forward) == {:error, :robot_not_found}
    end

    test "move robot with wrong movement", %{board: board} do
      board = %{board | robots: [%Robot{coordinate: %Coordinate{row: 1, col: 1}, face_direction: :right}]}
      assert Board.move_robot(board, 1, 1, :backwards) == {:error, :move_out_of_board}
    end

    test "move robot in busy coordinate", %{board: board} do
      robots = [
        %Robot{coordinate: %Coordinate{row: 1, col: 1}, face_direction: :right},
        %Robot{coordinate: %Coordinate{row: 1, col: 2}, face_direction: :right}
      ]
      board = %{board | robots: robots}
      assert Board.move_robot(board, 1, 1, :forward) == {:error, :overlapped_coordinate}
    end
  end

  describe "BoardGame.Board.turn_robot/4" do
    test "turn robot with valid coordinate and direction", %{board: board} do
      board = %{board | robots: [%Robot{coordinate: %Coordinate{row: 1, col: 1}, face_direction: :right}]}
      robot = %Robot{coordinate: %Coordinate{row: 1, col: 1}, face_direction: :up}
      assert Board.turn_robot(board, 1, 1, :left) == {:ok, robot, %{board | robots: [robot]}}
    end

    test "turn robot with invalid coordinate", %{board: board} do
      assert Board.turn_robot(board, 0, 0, :left) == {:error, :invalid_coordinate}
    end

    test "turn robot with invalid direction", %{board: board} do
      board = %{board | robots: [%Robot{coordinate: %Coordinate{row: 1, col: 1}, face_direction: :right}]}
      assert Board.turn_robot(board, 1, 1, :invalid) == {:error, :invalid_direction}
    end

    test "turn robot with not found robot", %{board: board} do
      assert Board.turn_robot(board, 1, 1, :right) == {:error, :robot_not_found}
    end
  end

  describe "BoardGame.Board.robot_attack/3" do
    test "turn robot with valid coordinate", %{board: board} do
      robot = %Robot{coordinate: %Coordinate{row: 2, col: 2}, face_direction: :right}
      dinosaur1 = %Dinosaur{coordinate: %Coordinate{row: 3, col: 3}, state: :alive}
      dinosaur2 = %Dinosaur{coordinate: %Coordinate{row: 3, col: 2}, state: :alive}
      dinosaur3 = %Dinosaur{coordinate: %Coordinate{row: 2, col: 3}, state: :alive}
      dinosaur4 = %Dinosaur{coordinate: %Coordinate{row: 2, col: 1}, state: :alive}
      dinosaur5 = %Dinosaur{coordinate: %Coordinate{row: 1, col: 2}, state: :alive}

      dead_dinosaurs = Enum.map([dinosaur2, dinosaur3, dinosaur4, dinosaur5],&Map.put(&1, :state, :dead))

      board = %{board | robots: [robot], dinosaurs: [dinosaur1 | dead_dinosaurs]}
      current_state = %{
        alive_dinosaurs: [dinosaur1],
        dead_dinosaurs: dead_dinosaurs,
        robots: [robot]
      }
      assert Board.robot_attack(board, 2, 2) == {:ok, current_state, %{
        board | robots: [robot], dinosaurs: dead_dinosaurs ++ [dinosaur1]}
      }
    end

    test "turn robot with invalid coordinate", %{board: board} do
      assert Board.robot_attack(board, 0, 0) == {:error, :invalid_coordinate}
    end

    test "turn robot with not found robot", %{board: board} do
      assert Board.robot_attack(board, 1, 1) == {:error, :robot_not_found}
    end
  end

  describe "BoardGame.Board.robot_instructions/3" do
    test "robot instructions with valid coordinate", %{board: board} do
      robot = %Robot{coordinate: %Coordinate{row: 1, col: 1}, face_direction: :right}
      board = %{board | robots: [robot]}
      assert Board.robot_instructions(board, 1, 1) == {:ok, %{
        coordinate: %Coordinate{row: 1, col: 1},
        possible_actions: [:move_forward, :turn_left, :turn_right, :attack],
        face_direction: :right
      }}

      robot = %Robot{coordinate: %Coordinate{row: 2, col: 2}, face_direction: :right}
      board = %{board | robots: [robot]}
      assert Board.robot_instructions(board, 2, 2) == {:ok, %{
        coordinate: %Coordinate{row: 2, col: 2},
        possible_actions: [:move_forward, :move_backwards, :turn_left, :turn_right, :attack],
        face_direction: :right
      }}

      robot = %Robot{coordinate: %Coordinate{row: 50, col: 50}, face_direction: :right}
      board = %{board | robots: [robot]}
      assert Board.robot_instructions(board, 50, 50) == {:ok, %{
        coordinate: %Coordinate{row: 50, col: 50},
        possible_actions: [:move_backwards, :turn_left, :turn_right, :attack],
        face_direction: :right
      }}
    end

    test "robot instructions with invalid coordinate", %{board: board} do
      assert Board.robot_instructions(board, 0, 0) == {:error, :invalid_coordinate}
    end

    test "robot instructions with not found robot", %{board: board} do
      assert Board.robot_instructions(board, 1, 1) == {:error, :robot_not_found}
    end
  end

  test "BoardGame.Board.current_state/1", %{board: board} do
    robot = %Robot{coordinate: %Coordinate{row: 2, col: 2}, face_direction: :right}
    dinosaur1 = %Dinosaur{coordinate: %Coordinate{row: 3, col: 3}, state: :alive}
    dinosaur2 = %Dinosaur{coordinate: %Coordinate{row: 3, col: 2}, state: :dead}
    dinosaur3 = %Dinosaur{coordinate: %Coordinate{row: 2, col: 3}, state: :dead}

    board = %{board | robots: [robot], dinosaurs: [dinosaur1, dinosaur2, dinosaur3]}
    assert Board.current_state(board) == {:ok, %{
      alive_dinosaurs: [dinosaur1],
      dead_dinosaurs: [dinosaur2, dinosaur3],
      robots: [robot]
    }}
  end
end
