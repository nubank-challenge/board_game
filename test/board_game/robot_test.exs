defmodule BoardGame.RobotTest do
  use ExUnit.Case, async: true

  alias BoardGame.{Robot, Coordinate}

  setup do
    {:ok, robot_up} = Robot.new(%Coordinate{row: 2, col: 2}, :up)
    {:ok, robot_down} = Robot.new(%Coordinate{row: 2, col: 2}, :down)
    {:ok, robot_left} = Robot.new(%Coordinate{row: 2, col: 2}, :left)
    {:ok, robot_right} = Robot.new(%Coordinate{row: 2, col: 2}, :right)
    {:ok, robot_up: robot_up, robot_down: robot_down, robot_left: robot_left, robot_right: robot_right}
  end

  describe "BoardGame.Robot.new/2" do
    test "with valid face_direction" do
      assert Robot.new(%Coordinate{row: 1, col: 50} , :up) == {:ok, %Robot{coordinate: %Coordinate{row: 1, col: 50}, face_direction: :up}}
      assert Robot.new(%Coordinate{row: 1, col: 50} , :down) == {:ok, %Robot{coordinate: %Coordinate{row: 1, col: 50}, face_direction: :down}}
      assert Robot.new(%Coordinate{row: 1, col: 50} , :left) == {:ok, %Robot{coordinate: %Coordinate{row: 1, col: 50}, face_direction: :left}}
      assert Robot.new(%Coordinate{row: 1, col: 50} , :right) == {:ok, %Robot{coordinate: %Coordinate{row: 1, col: 50}, face_direction: :right}}
    end

    test "with invalid face_direction" do
      assert Robot.new(%Coordinate{row: 1, col: 50} , :wrong) == {:error, :invalid_face_direction}
    end
  end

  describe "BoardGame.Robot.move/3" do
    test "move with success", %{robot_up: robot_up, robot_down: robot_down, robot_left: robot_left, robot_right: robot_right} do
      assert Robot.move(robot_up, :forward, [robot_up.coordinate]) == {:ok, %Robot{coordinate: %Coordinate{row: 3, col: 2}, face_direction: :up}}
      assert Robot.move(robot_down, :forward, [robot_down.coordinate]) == {:ok, %Robot{coordinate: %Coordinate{row: 1, col: 2}, face_direction: :down}}
      assert Robot.move(robot_left, :forward, [robot_left.coordinate]) == {:ok, %Robot{coordinate: %Coordinate{row: 2, col: 1}, face_direction: :left}}
      assert Robot.move(robot_right, :forward, [robot_right.coordinate]) == {:ok, %Robot{coordinate: %Coordinate{row: 2, col: 3}, face_direction: :right}}

      assert Robot.move(robot_up, :backwards, [robot_up.coordinate]) == {:ok, %Robot{coordinate: %Coordinate{row: 1, col: 2}, face_direction: :up}}
      assert Robot.move(robot_down, :backwards, [robot_down.coordinate]) == {:ok, %Robot{coordinate: %Coordinate{row: 3, col: 2}, face_direction: :down}}
      assert Robot.move(robot_left, :backwards, [robot_left.coordinate]) == {:ok, %Robot{coordinate: %Coordinate{row: 2, col: 3}, face_direction: :left}}
      assert Robot.move(robot_right, :backwards, [robot_right.coordinate]) == {:ok, %Robot{coordinate: %Coordinate{row: 2, col: 1}, face_direction: :right}}
    end

    test "move to busy coordinate", %{robot_up: robot} do
      assert Robot.move(robot, :forward, [%Coordinate{row: 3, col: 2}]) == {:error, :overlapped_coordinate}
    end

    test "move out of board" do
      {:ok, robot} = Robot.new(%Coordinate{row: 1, col: 1}, :down)
      assert Robot.move(robot, :forward, []) == {:error, :move_out_of_board}
    end
  end

  describe "BoardGame.Robot.turn/2" do
    test "turn with success", %{robot_up: robot_up, robot_down: robot_down, robot_left: robot_left, robot_right: robot_right} do
      assert Robot.turn(robot_up, :left) == {:ok, %Robot{coordinate: %Coordinate{row: 2, col: 2}, face_direction: :left}}
      assert Robot.turn(robot_down, :left) == {:ok, %Robot{coordinate: %Coordinate{row: 2, col: 2}, face_direction: :right}}
      assert Robot.turn(robot_left, :left) == {:ok, %Robot{coordinate: %Coordinate{row: 2, col: 2}, face_direction: :down}}
      assert Robot.turn(robot_right, :left) == {:ok, %Robot{coordinate: %Coordinate{row: 2, col: 2}, face_direction: :up}}

      assert Robot.turn(robot_up, :right) == {:ok, %Robot{coordinate: %Coordinate{row: 2, col: 2}, face_direction: :right}}
      assert Robot.turn(robot_down, :right) == {:ok, %Robot{coordinate: %Coordinate{row: 2, col: 2}, face_direction: :left}}
      assert Robot.turn(robot_left, :right) == {:ok, %Robot{coordinate: %Coordinate{row: 2, col: 2}, face_direction: :up}}
      assert Robot.turn(robot_right, :right) == {:ok, %Robot{coordinate: %Coordinate{row: 2, col: 2}, face_direction: :down}}
    end

    test "turn to invalid direction", %{robot_up: robot} do
      assert Robot.turn(robot, :wrong) == {:error, :invalid_direction}
    end
  end

  test "BoardGame.Robot.possible_moves/2", %{robot_up: robot} do
    assert Robot.possible_moves(robot, []) == [:forward, :backwards]
    assert Robot.possible_moves(robot, [%Coordinate{row: 3, col: 2}]) == [:backwards]
    assert Robot.possible_moves(robot, [%Coordinate{row: 1, col: 2}]) == [:forward]
    assert Robot.possible_moves(robot, [%Coordinate{row: 3, col: 2}, %Coordinate{row: 1, col: 2}]) == []
  end

  test "BoardGame.Robot.coordinates_around/1" do
    assert Robot.coordinates_around(%Robot{coordinate: %Coordinate{row: 2, col: 2}, face_direction: :up}) ==
      {:ok, [%Coordinate{row: 1, col: 2}, %Coordinate{row: 2, col: 1}, %Coordinate{row: 2, col: 3}, %Coordinate{row: 3, col: 2}]}
    assert Robot.coordinates_around(%Robot{coordinate: %Coordinate{row: 1, col: 1}, face_direction: :up}) ==
      {:ok, [%Coordinate{row: 1, col: 2}, %Coordinate{row: 2, col: 1}]}
    assert Robot.coordinates_around(%Robot{coordinate: %Coordinate{row: 50, col: 50}, face_direction: :up}) ==
      {:ok, [%Coordinate{row: 49, col: 50}, %Coordinate{row: 50, col: 49}]}
  end

  describe "BoardGame.Robot.find/2" do
    test "without robot with wanted coordinate", %{robot_up: robot} do
      assert Robot.find([robot], %Coordinate{row: 50, col: 50}) == {:error, :robot_not_found}
    end

    test "with robot with wanted coordinate", %{robot_up: robot} do
      assert Robot.find([robot], %Coordinate{row: 2, col: 2}) == {:ok, robot}
    end
  end

  describe "BoardGame.Robot.find_and_extract/2" do
    test "without robot with wanted coordinate", %{robot_up: robot} do
      assert Robot.find_and_extract([robot], %Coordinate{row: 50, col: 50}) == {:error, :robot_not_found}
    end

    test "with robot with wanted coordinate", %{robot_up: robot} do
      assert Robot.find_and_extract([robot], %Coordinate{row: 2, col: 2}) == {:ok, robot, []}
    end
  end

end
