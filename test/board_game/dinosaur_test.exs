defmodule BoardGame.DinosaurTest do
  use ExUnit.Case, async: true

  alias BoardGame.{Dinosaur, Coordinate}

  setup do
    dinosaurs =
      [[1, 1], [2, 2], [3, 3]]
      |> Enum.reduce([], fn [row, col], dinosaurs ->
        {:ok, coordinate} = Coordinate.new(row, col)
        {:ok, dinosaur} = Dinosaur.new(coordinate)
        [dinosaur | dinosaurs]
      end)
    {:ok, %{dinosaurs: dinosaurs}}
  end

  test "BoardGame.Dinosaur.new/1" do
    assert Dinosaur.new(%Coordinate{row: 1, col: 50}) == {:ok, %Dinosaur{coordinate: %Coordinate{row: 1, col: 50}, state: :alive}}
  end

  describe "BoardGame.Dinosaur.filter_by_state/2" do
    test "without dinosaurs with wanted state", %{dinosaurs: dinosaurs} do
      assert Dinosaur.filter_by_state(dinosaurs, :dead) == []
    end

    test "with dinosaurs with wanted state", %{dinosaurs: dinosaurs} do
      assert Dinosaur.filter_by_state(dinosaurs, :alive) == dinosaurs
    end
  end

  test "BoardGame.Dinosaur.under_attack/2", %{dinosaurs: [dinosaur | dinosaurs]} do
    attack_coordinates = [%Coordinate{row: 3, col: 3}]
    dinosaurs = [%{dinosaur | state: :dead} | dinosaurs]
    assert Dinosaur.under_attack(dinosaurs, attack_coordinates) == {:ok, dinosaurs}
  end

  describe "BoardGame.Dinosaur.find/3" do
    test "without dinosaur with wanted coordinate", %{dinosaurs: dinosaurs} do
      assert Dinosaur.find(dinosaurs, 4, 4) == {:error, :dinosaur_not_found}
    end

    test "with dinosaur with wanted coordinate", %{dinosaurs: [dinosaur | _] = dinosaurs} do
      assert Dinosaur.find(dinosaurs, 3, 3) == {:ok, dinosaur}
    end
  end

  describe "BoardGame.Dinosaur.find_and_extract/3" do
    test "without dinosaur with wanted coordinate", %{dinosaurs: dinosaurs} do
      assert Dinosaur.find_and_extract(dinosaurs, 4, 4) == {:error, :dinosaur_not_found}
    end

    test "with dinosaur with wanted coordinate", %{dinosaurs: [dinosaur | tail_dinosaurs] = dinosaurs} do
      assert Dinosaur.find_and_extract(dinosaurs, 3, 3) == {:ok, dinosaur, tail_dinosaurs}
    end
  end

end
