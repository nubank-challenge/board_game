defmodule BoardGameTest do
  use ExUnit.Case, async: false

  alias BoardGame.{Dinosaur, Robot, Coordinate}

  import Mock

  setup_with_mocks([
    {UUID, [], [uuid1: fn -> "1234567890" end]}
  ]) do
    on_exit(fn -> BoardGame.destroy(:"1234567890") end)
    {:ok, game_id: :"1234567890"}
  end

  test "BoardGame.create/0",%{game_id: game_id} do
    assert BoardGame.create == {:ok, game_id}
    assert Process.whereis(game_id) != nil
  end

  test "BoardGame.destroy/1",%{game_id: game_id} do
    assert BoardGame.create == {:ok, game_id}
    assert BoardGame.destroy(game_id) == :ok
    assert Process.whereis(game_id) == nil
  end

  describe "BoardGame.position_dinosaur/3" do
    setup do
      {:ok, _game_id} = BoardGame.create
      :ok
    end

    test "position dinosaur with valid coordinate", %{game_id: game_id} do
      dinosaur = %Dinosaur{coordinate: %Coordinate{row: 1, col: 1}, state: :alive}
      assert BoardGame.position_dinosaur(game_id, 1, 1) == {:ok, dinosaur}
    end

    test "position dinosaur with invalid coordinate", %{game_id: game_id} do
      assert BoardGame.position_dinosaur(game_id, 0, 0) == {:error, :invalid_coordinate}
    end

    test "position dinosaur in busy coordinate", %{game_id: game_id} do
      BoardGame.position_dinosaur(game_id, 1, 1)
      assert BoardGame.position_dinosaur(game_id, 1, 1) == {:error, :overlapped_coordinate}
    end
  end

  describe "BoardGame.position_robot/4" do
    setup do
      {:ok, _game_id} = BoardGame.create
      :ok
    end

    test "position robot with valid coordinate and face_direction", %{game_id: game_id} do
      robot = %Robot{coordinate: %Coordinate{row: 1, col: 1}, face_direction: :right}
      assert BoardGame.position_robot(game_id, 1, 1, :right) == {:ok, robot}
    end

    test "position robot with invalid coordinate", %{game_id: game_id} do
      assert BoardGame.position_robot(game_id, 0, 0, :right) == {:error, :invalid_coordinate}
    end

    test "position robot with invalid face_direction", %{game_id: game_id} do
      assert BoardGame.position_robot(game_id, 1, 1, :wrong) == {:error, :invalid_face_direction}
    end

    test "position robot in busy coordinate", %{game_id: game_id} do
      BoardGame.position_robot(game_id, 1, 1, :right)
      assert BoardGame.position_robot(game_id, 1, 1, :right) == {:error, :overlapped_coordinate}
    end
  end

  describe "BoardGame.move_robot/4" do
    setup do
      {:ok, _game_id} = BoardGame.create
      :ok
    end

    test "move robot with valid coordinate and movement", %{game_id: game_id} do
      BoardGame.position_robot(game_id, 1, 1, :right)
      expected_robot = %Robot{coordinate: %Coordinate{row: 1, col: 2}, face_direction: :right}
      assert BoardGame.move_robot(game_id, 1, 1, :forward) == {:ok, expected_robot}
    end

    test "move robot with invalid coordinate", %{game_id: game_id} do
      assert BoardGame.move_robot(game_id, 0, 0, :forward) == {:error, :invalid_coordinate}
    end

    test "move robot with invalid movement", %{game_id: game_id} do
      BoardGame.position_robot(game_id, 1, 1, :right)
      assert BoardGame.move_robot(game_id, 1, 1, :invalid) == {:error, :invalid_movement}
    end

    test "move robot with not found robot", %{game_id: game_id} do
      assert BoardGame.move_robot(game_id, 1, 1, :forward) == {:error, :robot_not_found}
    end

    test "move robot with wrong movement", %{game_id: game_id} do
      BoardGame.position_robot(game_id, 1, 1, :right)
      assert BoardGame.move_robot(game_id, 1, 1, :backwards) == {:error, :move_out_of_board}
    end

    test "move robot in busy coordinate", %{game_id: game_id} do
      BoardGame.position_robot(game_id, 1, 1, :right)
      BoardGame.position_robot(game_id, 1, 2, :right)
      assert BoardGame.move_robot(game_id, 1, 1, :forward) == {:error, :overlapped_coordinate}
    end
  end

  describe "BoardGame.turn_robot/4" do
    setup do
      {:ok, _game_id} = BoardGame.create
      :ok
    end

    test "turn robot with valid coordinate and direction", %{game_id: game_id} do
      BoardGame.position_robot(game_id, 1, 1, :right)
      expected_robot = %Robot{coordinate: %Coordinate{row: 1, col: 1}, face_direction: :up}
      assert BoardGame.turn_robot(game_id, 1, 1, :left) == {:ok, expected_robot}
    end

    test "turn robot with invalid coordinate", %{game_id: game_id} do
      assert BoardGame.turn_robot(game_id, 0, 0, :left) == {:error, :invalid_coordinate}
    end

    test "turn robot with invalid direction", %{game_id: game_id} do
      BoardGame.position_robot(game_id, 1, 1, :right)
      assert BoardGame.turn_robot(game_id, 1, 1, :invalid) == {:error, :invalid_direction}
    end

    test "turn robot with not found robot", %{game_id: game_id} do
      assert BoardGame.turn_robot(game_id, 1, 1, :right) == {:error, :robot_not_found}
    end
  end

  describe "BoardGame.robot_attack/3" do
    setup do
      {:ok, _game_id} = BoardGame.create
      :ok
    end

    test "turn robot with valid coordinate", %{game_id: game_id} do
      BoardGame.position_robot(game_id, 2, 2, :right)
      BoardGame.position_dinosaur(game_id, 3, 3)
      BoardGame.position_dinosaur(game_id, 3, 2)
      BoardGame.position_dinosaur(game_id, 2, 3)
      BoardGame.position_dinosaur(game_id, 2, 1)
      BoardGame.position_dinosaur(game_id, 1, 2)

      robot = %Robot{coordinate: %Coordinate{row: 2, col: 2}, face_direction: :right}
      dinosaur1 = %Dinosaur{coordinate: %Coordinate{row: 3, col: 3}, state: :alive}
      dinosaur2 = %Dinosaur{coordinate: %Coordinate{row: 3, col: 2}, state: :dead}
      dinosaur3 = %Dinosaur{coordinate: %Coordinate{row: 2, col: 3}, state: :dead}
      dinosaur4 = %Dinosaur{coordinate: %Coordinate{row: 2, col: 1}, state: :dead}
      dinosaur5 = %Dinosaur{coordinate: %Coordinate{row: 1, col: 2}, state: :dead}

      expected_current_state = %{
        alive_dinosaurs: [dinosaur1],
        dead_dinosaurs: [dinosaur2, dinosaur3, dinosaur4, dinosaur5],
        robots: [robot]
      }
      assert BoardGame.robot_attack(game_id, 2, 2) == {:ok, expected_current_state}
    end

    test "turn robot with invalid coordinate", %{game_id: game_id} do
      assert BoardGame.robot_attack(game_id, 0, 0) == {:error, :invalid_coordinate}
    end

    test "turn robot with not found robot", %{game_id: game_id} do
      assert BoardGame.robot_attack(game_id, 1, 1) == {:error, :robot_not_found}
    end
  end

  describe "BoardGame.robot_instructions/3" do
    setup do
      {:ok, _game_id} = BoardGame.create
      :ok
    end

    test "robot instructions with valid coordinate", %{game_id: game_id} do
      BoardGame.position_robot(game_id, 1, 1, :right)
      assert BoardGame.robot_instructions(game_id, 1, 1) == {:ok, %{
        coordinate: %Coordinate{row: 1, col: 1},
        possible_actions: [:move_forward, :turn_left, :turn_right, :attack],
        face_direction: :right
      }}

      BoardGame.position_robot(game_id, 2, 2, :right)
      assert BoardGame.robot_instructions(game_id, 2, 2) == {:ok, %{
        coordinate: %Coordinate{row: 2, col: 2},
        possible_actions: [:move_forward, :move_backwards, :turn_left, :turn_right, :attack],
        face_direction: :right
      }}

      BoardGame.position_robot(game_id, 50, 50, :right)
      assert BoardGame.robot_instructions(game_id, 50, 50) == {:ok, %{
        coordinate: %Coordinate{row: 50, col: 50},
        possible_actions: [:move_backwards, :turn_left, :turn_right, :attack],
        face_direction: :right
      }}
    end

    test "robot instructions with invalid coordinate", %{game_id: game_id} do
      assert BoardGame.robot_instructions(game_id, 0, 0) == {:error, :invalid_coordinate}
    end

    test "robot instructions with not found robot", %{game_id: game_id} do
      assert BoardGame.robot_instructions(game_id, 1, 1) == {:error, :robot_not_found}
    end
  end

  test "BoardGame.Board.current_state/1", %{game_id: game_id} do
    BoardGame.create
    BoardGame.position_robot(game_id, 2, 2, :right)
    BoardGame.position_dinosaur(game_id, 3, 3)
    BoardGame.position_dinosaur(game_id, 3, 2)
    BoardGame.position_dinosaur(game_id, 2, 3)
    BoardGame.robot_attack(game_id, 2, 2)

    robot = %Robot{coordinate: %Coordinate{row: 2, col: 2}, face_direction: :right}
    dinosaur1 = %Dinosaur{coordinate: %Coordinate{row: 3, col: 3}, state: :alive}
    dinosaur2 = %Dinosaur{coordinate: %Coordinate{row: 3, col: 2}, state: :dead}
    dinosaur3 = %Dinosaur{coordinate: %Coordinate{row: 2, col: 3}, state: :dead}

    assert BoardGame.current_state(game_id) == {:ok, %{
      alive_dinosaurs: [dinosaur1],
      dead_dinosaurs: [dinosaur2, dinosaur3],
      robots: [robot]
    }}
  end
end
