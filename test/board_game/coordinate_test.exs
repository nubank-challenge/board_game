defmodule BoardGame.CoordinateTest do
  use ExUnit.Case, async: true

  alias BoardGame.Coordinate

  describe "BoardGame.Coordinate.new/2" do
    test "arguments between 1 and 50" do
      assert Coordinate.new(1, 50) == {:ok, %Coordinate{row: 1, col: 50}}
    end

    test "arguments below 1" do
      assert Coordinate.new(1, 0) == {:error, :invalid_coordinate}
      assert Coordinate.new(0, 50) == {:error, :invalid_coordinate}
    end

    test "arguments above 50" do
      assert Coordinate.new(1, 51) == {:error, :invalid_coordinate}
      assert Coordinate.new(51, 50) == {:error, :invalid_coordinate}
    end
  end

  describe "BoardGame.Coordinate.available?/2" do
    setup do
      coordinates =
        [[1, 1], [2, 2], [3, 3]]
        |> Enum.reduce([], fn [row, col], coordinates ->
          {:ok, coordinate} = Coordinate.new(row, col)
          [coordinate | coordinates]
        end)
      {:ok, %{coordinates: coordinates}}
    end

    test "current coordinates includes coordinate", %{coordinates: coordinates} do
      assert Coordinate.available?(coordinates, %Coordinate{row: 1, col: 1}) == {:error, :overlapped_coordinate}
    end

    test "current coordinates excludes coordinate", %{coordinates: coordinates} do
      assert Coordinate.available?(coordinates, %Coordinate{row: 1, col: 50}) == :ok
    end
  end

end
