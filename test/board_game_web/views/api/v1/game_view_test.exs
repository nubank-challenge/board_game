defmodule BoardGameWeb.Api.V1.GameViewTest do
  use BoardGameWeb.ConnCase, async: true

  alias BoardGame.{Dinosaur, Robot, Coordinate}
  # Bring render/3 and render_to_string/3 for testing custom views
  import Phoenix.View


  test "create.json" do
    assert render(BoardGameWeb.Api.V1.GameView, "create.json", game_id: :test) == %{game_id: :test}
  end

  test "current_state.json" do
    current_state = %{
      alive_dinosaurs: [%Dinosaur{coordinate: %Coordinate{row: 1, col: 1}, state: :alive}],
      dead_dinosaurs: [%Dinosaur{coordinate: %Coordinate{row: 2, col: 2}, state: :dead}],
      robots: [%Robot{coordinate: %Coordinate{row: 3, col: 3}, face_direction: :right}]
    }

    assert render(BoardGameWeb.Api.V1.GameView, "current_state.json", current_state: current_state) == %{
      dinosaurs: %{
        alive: [%{coordinate: %{row: 1, col: 1}}],
        dead: [%{coordinate: %{row: 2, col: 2}}]
      },
      robots: [%{coordinate: %{row: 3, col: 3}, face_direction: :right}]
    }
  end

  test "dinosaur.json" do
    dinosaur = %Dinosaur{coordinate: %Coordinate{row: 1, col: 1}, state: :alive}
    assert render(BoardGameWeb.Api.V1.GameView, "dinosaur.json", dinosaur: dinosaur) == %{
      dinosaur: %{coordinate: %{row: 1, col: 1}}
    }
  end

  test "dinosaur" do
    dinosaur = %Dinosaur{coordinate: %Coordinate{row: 1, col: 1}, state: :alive}
    assert render(BoardGameWeb.Api.V1.GameView, "dinosaur", dinosaur: dinosaur) == %{coordinate: %{row: 1, col: 1}}
  end

  test "robot.json" do
    robot = %Robot{coordinate: %Coordinate{row: 1, col: 1}, face_direction: :right}
    assert render(BoardGameWeb.Api.V1.GameView, "robot.json", robot: robot) == %{
      robot: %{coordinate: %{row: 1, col: 1}, face_direction: :right}
    }
  end

  test "robot" do
    robot = %Robot{coordinate: %Coordinate{row: 1, col: 1}, face_direction: :right}
    assert render(BoardGameWeb.Api.V1.GameView, "robot", robot: robot) == %{coordinate: %{row: 1, col: 1}, face_direction: :right}
  end

  test "robot_instructions.json" do
    instructions = %{
      coordinate: %Coordinate{row: 1, col: 1},
      possible_actions: [:move_forward, :move_backwards, :turn_left, :turn_right, :attack],
      face_direction: :up
    }

    assert render(BoardGameWeb.Api.V1.GameView, "robot_instructions.json", instructions: instructions) == %{
      coordinate: %{row: 1, col: 1},
      possible_actions: [:move_forward, :move_backwards, :turn_left, :turn_right, :attack],
      face_direction: :up
    }
  end

  describe "error.json" do
    test "not_found" do
      assert render(BoardGameWeb.Api.V1.GameView, "error.json", error: :not_found) == %{
        code: :not_found, error: "Resource not found"
      }
    end

    test "robot_not_found" do
      assert render(BoardGameWeb.Api.V1.GameView, "error.json", error: :robot_not_found) == %{
        code: :robot_not_found, error: "Robot not found"
      }
    end

    test "missing_coordinate" do
      assert render(BoardGameWeb.Api.V1.GameView, "error.json", error: :missing_coordinate) == %{
        code: :missing_coordinate, error: "Coordinate required"
      }
    end

    test "missing_face_direction" do
      assert render(BoardGameWeb.Api.V1.GameView, "error.json", error: :missing_face_direction) == %{
        code: :missing_face_direction, error: "Face direction required"
      }
    end

    test "missing_robot_action" do
      assert render(BoardGameWeb.Api.V1.GameView, "error.json", error: :missing_robot_action) == %{
        code: :missing_robot_action, error: "Robot action required"
      }
    end

    test "invalid_coordinate" do
      assert render(BoardGameWeb.Api.V1.GameView, "error.json", error: :invalid_coordinate) == %{
        code: :invalid_coordinate, error: "Invalid coordinate"
      }
    end

    test "invalid_face_direction" do
      assert render(BoardGameWeb.Api.V1.GameView, "error.json", error: :invalid_face_direction) == %{
        code: :invalid_face_direction, error: "Invalid face direction"
      }
    end

    test "invalid_robot_action" do
      assert render(BoardGameWeb.Api.V1.GameView, "error.json", error: :invalid_robot_action) == %{
        code: :invalid_robot_action, error: "Invalid robot action"
      }
    end

    test "invalid_movement" do
      assert render(BoardGameWeb.Api.V1.GameView, "error.json", error: :invalid_movement) == %{
        code: :invalid_movement, error: "Invalid movement"
      }
    end

    test "invalid_direction" do
      assert render(BoardGameWeb.Api.V1.GameView, "error.json", error: :invalid_direction) == %{
        code: :invalid_direction, error: "Invalid direction"
      }
    end

    test "overlapped_coordinate" do
      assert render(BoardGameWeb.Api.V1.GameView, "error.json", error: :overlapped_coordinate) == %{
        code: :overlapped_coordinate, error: "Overlapped coordinate"
      }
    end

    test "move_out_of_board" do
      assert render(BoardGameWeb.Api.V1.GameView, "error.json", error: :move_out_of_board) == %{
        code: :move_out_of_board, error: "Move out of board"
      }
    end

    test "internal_server_error" do
      assert render(BoardGameWeb.Api.V1.GameView, "error.json", error: :internal_server_error) == %{
        code: :internal_server_error, error: "Internal Server Error"
      }
    end

    test "other bitstring error" do
      assert render(BoardGameWeb.Api.V1.GameView, "error.json", error: "other error") == %{
        code: "other error", error: "Other error"
      }
    end

    test "other atom error" do
      assert render(BoardGameWeb.Api.V1.GameView, "error.json", error: :unkwon_error) == %{
        code: :unkwon_error, error: "Unkwon error"
      }
    end
  end
end
