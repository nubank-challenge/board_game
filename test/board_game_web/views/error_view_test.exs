defmodule BoardGameWeb.ErrorViewTest do
  use BoardGameWeb.ConnCase, async: true

  # Bring render/3 and render_to_string/3 for testing custom views
  import Phoenix.View

  test "renders 404.json" do
    assert render(BoardGameWeb.ErrorView, "404.json", []) == %{code: :not_found, error: "Not Found"}
  end

  test "renders 500.json" do
    assert render(BoardGameWeb.ErrorView, "500.json", []) == %{code: :internal_server_error, error: "Internal Server Error"}
  end
end
