defmodule BoardGameWeb.Api.V1.HealthControllerTest do
  use BoardGameWeb.ConnCase, async: false

  import Mock

  describe "health" do
    test "sucess", %{conn: conn} do
      response =
        conn
        |> get(Routes.api_v1_health_path(conn, :health))
        |> json_response(:ok)

      expected = %{"success" => true, "games" => 0}
      assert response == expected
    end

    test "error", %{conn: conn} do
      with_mock(BoardGame.DynamicSupervisor, [
        count_children: fn -> :unkwon_error end
      ]) do
        response =
          conn
          |> get(Routes.api_v1_health_path(conn, :health))
          |> json_response(:internal_server_error)

        expected = %{"success" => false, "error" => ":unkwon_error"}
        assert response == expected
      end
    end
  end
end
