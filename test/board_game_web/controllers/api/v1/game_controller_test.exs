defmodule BoardGameWeb.Api.V1.GameControllerTest do
  use BoardGameWeb.ConnCase, async: false

  import Mock

  setup_with_mocks([
    {UUID, [], [uuid1: fn -> "1234567890" end]}
  ]) do
    on_exit(fn -> BoardGame.destroy(:"1234567890") end)
    {:ok, game_id: "1234567890"}
  end

  describe "create" do
    test "responds with sucess", %{conn: conn, game_id: game_id} do
      response =
        conn
        |> post(Routes.api_v1_game_path(conn, :create))
        |> json_response(:created)

      expected = %{"game_id" => game_id}
      assert response == expected
    end

    test "responds with error", %{conn: conn} do
      with_mock(BoardGame, [
        create: fn -> {:error, :internal_server_error} end
      ]) do
        response =
          conn
          |> post(Routes.api_v1_game_path(conn, :create))
          |> json_response(:internal_server_error)

        expected = %{"code" => "internal_server_error", "error" => "Internal Server Error"}
        assert response == expected
      end
    end
  end

  describe "delete" do
    setup do
      BoardGame.create
      :ok
    end

    test "responds with sucess", %{conn: conn, game_id: game_id} do
      response =
        conn
        |> delete(Routes.api_v1_game_path(conn, :delete, game_id))
        |> response(:no_content)

      assert response == ""
    end

    test "responds with error", %{conn: conn, game_id: game_id} do
      with_mock(BoardGame, [
        destroy: fn _ -> {:error, :internal_server_error} end
      ]) do
        response =
          conn
          |> delete(Routes.api_v1_game_path(conn, :delete, game_id))
          |> json_response(:internal_server_error)

        expected = %{"code" => "internal_server_error", "error" => "Internal Server Error"}
        assert response == expected
      end
    end
  end

  describe "show" do
    setup %{game_id: game_id} do
      BoardGame.create
      BoardGame.position_robot(:"#{game_id}", 2, 2, :right)
      BoardGame.position_dinosaur(:"#{game_id}", 3, 3)
      BoardGame.position_dinosaur(:"#{game_id}", 3, 2)
      BoardGame.position_dinosaur(:"#{game_id}", 2, 3)
      BoardGame.robot_attack(:"#{game_id}", 2, 2)
      :ok
    end

    test "responds with sucess", %{conn: conn, game_id: game_id} do
      response =
        conn
        |> get(Routes.api_v1_game_path(conn, :show, game_id))
        |> json_response(:ok)

      expected = %{
        "dinosaurs" => %{
          "alive" => [%{"coordinate" => %{"col" => 3, "row" => 3}}],
          "dead" => [
            %{"coordinate" => %{"col" => 2, "row" => 3}},
            %{"coordinate" => %{"col" => 3, "row" => 2}}]
        },
        "robots" => [%{"coordinate" => %{"col" => 2, "row" => 2}, "face_direction" => "right"}]
      }
      assert response == expected
    end

    test "responds with not_found", %{conn: conn} do
      response =
        conn
        |> get(Routes.api_v1_game_path(conn, :show, "not_found"))
        |> json_response(:not_found)

      expected = %{"code" => "not_found", "error" => "Resource not found"}
      assert response == expected
    end
  end

  describe "position_dinosaur" do
    setup do
      BoardGame.create
      :ok
    end

    test "responds with sucess", %{conn: conn, game_id: game_id} do
      response =
        conn
        |> post(Routes.api_v1_game_game_path(conn, :position_dinosaur, game_id), coordinate: %{row: 1, col: 1})
        |> json_response(:created)

      expected = %{"dinosaur" => %{"coordinate" => %{"col" => 1, "row" => 1}}}
      assert response == expected
    end

    test "responds with invalid coordinate error", %{conn: conn, game_id: game_id} do
      response =
        conn
        |> post(Routes.api_v1_game_game_path(conn, :position_dinosaur, game_id), coordinate: %{row: 0, col: 1})
        |> json_response(:unprocessable_entity)

      expected = %{"code" => "invalid_coordinate", "error" => "Invalid coordinate"}
      assert response == expected
    end

    test "responds with overlapped coordinate error", %{conn: conn, game_id: game_id} do
      BoardGame.position_dinosaur(:"#{game_id}", 1, 1)

      response =
        conn
        |> post(Routes.api_v1_game_game_path(conn, :position_dinosaur, game_id), coordinate: %{row: 1, col: 1})
        |> json_response(:unprocessable_entity)

      expected = %{"code" => "overlapped_coordinate", "error" => "Overlapped coordinate"}
      assert response == expected
    end

    test "responds with missing coordinate error", %{conn: conn, game_id: game_id} do
      response =
        conn
        |> post(Routes.api_v1_game_game_path(conn, :position_dinosaur, game_id))
        |> json_response(:bad_request)

      expected = %{"code" => "missing_coordinate", "error" => "Coordinate required"}
      assert response == expected
    end

    test "responds with not_found", %{conn: conn} do
      response =
        conn
        |> post(Routes.api_v1_game_game_path(conn, :position_dinosaur, "not_found"), coordinate: %{row: 1, col: 1})
        |> json_response(:not_found)

      expected = %{"code" => "not_found", "error" => "Resource not found"}
      assert response == expected
    end
  end

  describe "position_robot" do
    setup do
      BoardGame.create
      :ok
    end

    test "responds with sucess", %{conn: conn, game_id: game_id} do
      response =
        conn
        |> post(Routes.api_v1_game_game_path(conn, :position_robot, game_id), coordinate: %{row: 1, col: 1}, face_direction: "up")
        |> json_response(:created)

      expected = %{"robot" => %{"coordinate" => %{"col" => 1, "row" => 1}, "face_direction" => "up"}}
      assert response == expected
    end

    test "responds with invalid coordinate error", %{conn: conn, game_id: game_id} do
      response =
        conn
        |> post(Routes.api_v1_game_game_path(conn, :position_robot, game_id), coordinate: %{row: 0, col: 1}, face_direction: "up")
        |> json_response(:unprocessable_entity)

      expected = %{"code" => "invalid_coordinate", "error" => "Invalid coordinate"}
      assert response == expected
    end

    test "responds with overlapped coordinate error", %{conn: conn, game_id: game_id} do
      BoardGame.position_robot(:"#{game_id}", 1, 1, :up)

      response =
        conn
        |> post(Routes.api_v1_game_game_path(conn, :position_robot, game_id), coordinate: %{row: 1, col: 1}, face_direction: "up")
        |> json_response(:unprocessable_entity)

      expected = %{"code" => "overlapped_coordinate", "error" => "Overlapped coordinate"}
      assert response == expected
    end

    test "responds with missing coordinate error", %{conn: conn, game_id: game_id} do
      response =
        conn
        |> post(Routes.api_v1_game_game_path(conn, :position_robot, game_id), face_direction: "up")
        |> json_response(:bad_request)

      expected = %{"code" => "missing_coordinate", "error" => "Coordinate required"}
      assert response == expected
    end

    test "responds with missing face_direction error", %{conn: conn, game_id: game_id} do
      response =
        conn
        |> post(Routes.api_v1_game_game_path(conn, :position_robot, game_id), coordinate: %{row: 1, col: 1})
        |> json_response(:bad_request)

      expected = %{"code" => "missing_face_direction", "error" => "Face direction required"}
      assert response == expected
    end

    test "responds with not_found", %{conn: conn} do
      response =
        conn
        |> post(Routes.api_v1_game_game_path(conn, :position_robot, "not_found"), coordinate: %{row: 1, col: 1}, face_direction: "up")
        |> json_response(:not_found)

      expected = %{"code" => "not_found", "error" => "Resource not found"}
      assert response == expected
    end
  end

  describe "robot_action" do
    setup do
      BoardGame.create
      :ok
    end

    test "responds with sucess", %{conn: conn, game_id: game_id} do
      BoardGame.position_robot(:"#{game_id}", 1, 2, :up)

      response =
        conn
        |> put(Routes.api_v1_game_game_path(conn, :robot_action, game_id), coordinate: %{row: 1, col: 2}, action: "move_forward")
        |> json_response(:ok)

      expected = %{"robot" => %{"coordinate" => %{"col" => 2, "row" => 2}, "face_direction" => "up"}}
      assert response == expected

      response =
        conn
        |> put(Routes.api_v1_game_game_path(conn, :robot_action, game_id), coordinate: %{row: 2, col: 2}, action: "turn_left")
        |> json_response(:ok)

      expected = %{"robot" => %{"coordinate" => %{"col" => 2, "row" => 2}, "face_direction" => "left"}}
      assert response == expected

      BoardGame.position_dinosaur(:"#{game_id}", 3, 3)
      BoardGame.position_dinosaur(:"#{game_id}", 3, 2)
      BoardGame.position_dinosaur(:"#{game_id}", 2, 3)
      BoardGame.position_dinosaur(:"#{game_id}", 2, 1)
      BoardGame.position_dinosaur(:"#{game_id}", 1, 2)

      response =
        conn
        |> put(Routes.api_v1_game_game_path(conn, :robot_action, game_id), coordinate: %{row: 2, col: 2}, action: "attack")
        |> json_response(:ok)

      expected = %{
        "dinosaurs" => %{
          "alive" => [%{"coordinate" => %{"col" => 3, "row" => 3}}],
          "dead" => [
            %{"coordinate" => %{"col" => 2, "row" => 3}},
            %{"coordinate" => %{"col" => 3, "row" => 2}},
            %{"coordinate" => %{"col" => 1, "row" => 2}},
            %{"coordinate" => %{"col" => 2, "row" => 1}}
          ]
        },
        "robots" => [%{"coordinate" => %{"col" => 2, "row" => 2}, "face_direction" => "left"}]
      }
      assert response == expected
    end

    test "responds with invalid coordinate error", %{conn: conn, game_id: game_id} do
      response =
        conn
        |> put(Routes.api_v1_game_game_path(conn, :position_robot, game_id), coordinate: %{row: 0, col: 1}, action: "move_forward")
        |> json_response(:unprocessable_entity)

      expected = %{"code" => "invalid_coordinate", "error" => "Invalid coordinate"}
      assert response == expected
    end

    test "responds with invalid movement error", %{conn: conn, game_id: game_id} do
      BoardGame.position_robot(:"#{game_id}", 1, 1, :up)

      response =
        conn
        |> put(Routes.api_v1_game_game_path(conn, :position_robot, game_id), coordinate: %{row: 1, col: 1}, action: "move_right")
        |> json_response(:unprocessable_entity)

      expected = %{"code" => "invalid_movement", "error" => "Invalid movement"}
      assert response == expected
    end

    test "responds with invalid direction error", %{conn: conn, game_id: game_id} do
      BoardGame.position_robot(:"#{game_id}", 1, 1, :up)

      response =
        conn
        |> put(Routes.api_v1_game_game_path(conn, :position_robot, game_id), coordinate: %{row: 1, col: 1}, action: "turn_up")
        |> json_response(:unprocessable_entity)

      expected = %{"code" => "invalid_direction", "error" => "Invalid direction"}
      assert response == expected
    end

    test "responds with move out of board error", %{conn: conn, game_id: game_id} do
      BoardGame.position_robot(:"#{game_id}", 1, 1, :up)

      response =
        conn
        |> put(Routes.api_v1_game_game_path(conn, :position_robot, game_id), coordinate: %{row: 1, col: 1}, action: "move_backwards")
        |> json_response(:unprocessable_entity)

      expected = %{"code" => "move_out_of_board", "error" => "Move out of board"}
      assert response == expected
    end

    test "responds with overlapped coordinate error", %{conn: conn, game_id: game_id} do
      BoardGame.position_robot(:"#{game_id}", 1, 1, :up)
      BoardGame.position_robot(:"#{game_id}", 2, 1, :up)

      response =
        conn
        |> put(Routes.api_v1_game_game_path(conn, :position_robot, game_id), coordinate: %{row: 1, col: 1}, action: "move_forward")
        |> json_response(:unprocessable_entity)

      expected = %{"code" => "overlapped_coordinate", "error" => "Overlapped coordinate"}
      assert response == expected
    end

    test "responds with missing coordinate error", %{conn: conn, game_id: game_id} do
      response =
        conn
        |> put(Routes.api_v1_game_game_path(conn, :position_robot, game_id), action: "move_forward")
        |> json_response(:bad_request)

      expected = %{"code" => "missing_coordinate", "error" => "Coordinate required"}
      assert response == expected
    end

    test "responds with missing robot_action error", %{conn: conn, game_id: game_id} do
      response =
        conn
        |> put(Routes.api_v1_game_game_path(conn, :position_robot, game_id), coordinate: %{row: 1, col: 1})
        |> json_response(:bad_request)

      expected = %{"code" => "missing_robot_action", "error" => "Robot action required"}
      assert response == expected
    end

    test "responds with invalid robot_action error", %{conn: conn, game_id: game_id} do
      response =
        conn
        |> put(Routes.api_v1_game_game_path(conn, :position_robot, game_id), coordinate: %{row: 1, col: 1}, action: "invalid")
        |> json_response(:unprocessable_entity)

      expected = %{"code" => "invalid_robot_action", "error" => "Invalid robot action"}
      assert response == expected
    end

    test "responds with robot not_found error", %{conn: conn, game_id: game_id} do
      response =
        conn
        |> put(Routes.api_v1_game_game_path(conn, :position_robot, game_id), coordinate: %{row: 1, col: 1}, action: "move_forward")
        |> json_response(:not_found)

      expected = %{"code" => "robot_not_found", "error" => "Robot not found"}
      assert response == expected
    end

    test "responds with not_found", %{conn: conn} do
      response =
        conn
        |> put(Routes.api_v1_game_game_path(conn, :position_robot, "not_found"), coordinate: %{row: 1, col: 1}, action: "move_forward")
        |> json_response(:not_found)

      expected = %{"code" => "not_found", "error" => "Resource not found"}
      assert response == expected
    end
  end

  describe "robot_instructions" do
    setup do
      BoardGame.create
      :ok
    end

    test "responds with sucess", %{conn: conn, game_id: game_id} do
      BoardGame.position_robot(:"#{game_id}", 1, 1, :up)
      BoardGame.position_robot(:"#{game_id}", 25, 25, :up)
      BoardGame.position_robot(:"#{game_id}", 50, 50, :up)

      response =
        conn
        |> post(Routes.api_v1_game_game_path(conn, :robot_instructions, game_id), coordinate: %{row: 1, col: 1})
        |> json_response(:ok)

      expected = %{
        "coordinate" => %{"col" => 1, "row" => 1},
        "face_direction" => "up",
        "possible_actions" => ["move_forward", "turn_left", "turn_right", "attack"]
      }
      assert response == expected

      response =
        conn
        |> post(Routes.api_v1_game_game_path(conn, :robot_instructions, game_id), coordinate: %{row: 25, col: 25})
        |> json_response(:ok)

      expected = %{
        "coordinate" => %{"col" => 25, "row" => 25},
        "face_direction" => "up",
        "possible_actions" => ["move_forward", "move_backwards", "turn_left", "turn_right", "attack"]
      }
      assert response == expected

      response =
        conn
        |> post(Routes.api_v1_game_game_path(conn, :robot_instructions, game_id), coordinate: %{row: 50, col: 50})
        |> json_response(:ok)

      expected = %{
        "coordinate" => %{"col" => 50, "row" => 50},
        "face_direction" => "up",
        "possible_actions" => ["move_backwards", "turn_left", "turn_right", "attack"]
      }
      assert response == expected
    end

    test "responds with invalid coordinate error", %{conn: conn, game_id: game_id} do
      response =
        conn
        |> post(Routes.api_v1_game_game_path(conn, :robot_instructions, game_id), coordinate: %{row: 0, col: 1})
        |> json_response(:unprocessable_entity)

      expected = %{"code" => "invalid_coordinate", "error" => "Invalid coordinate"}
      assert response == expected
    end

    test "responds with robot not_found error", %{conn: conn, game_id: game_id} do
      response =
        conn
        |> post(Routes.api_v1_game_game_path(conn, :robot_instructions, game_id), coordinate: %{row: 1, col: 1})
        |> json_response(:not_found)

      expected = %{"code" => "robot_not_found", "error" => "Robot not found"}
      assert response == expected
    end

    test "responds with missing coordinate error", %{conn: conn, game_id: game_id} do
      response =
        conn
        |> post(Routes.api_v1_game_game_path(conn, :robot_instructions, game_id))
        |> json_response(:bad_request)

      expected = %{"code" => "missing_coordinate", "error" => "Coordinate required"}
      assert response == expected
    end

    test "responds with not_found", %{conn: conn} do
      response =
        conn
        |> post(Routes.api_v1_game_game_path(conn, :robot_instructions, "not_found"), coordinate: %{row: 1, col: 1})
        |> json_response(:not_found)

      expected = %{"code" => "not_found", "error" => "Resource not found"}
      assert response == expected
    end
  end
end
