# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :board_game, BoardGameWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "hMXDkgXf8iUzYr+RuksUpx5pwR3FLUZzzCy006uO5HFm9kn30HBtD0tsgQEF/dQQ",
  render_errors: [view: BoardGameWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: BoardGame.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$date $time [$level] $message\n",
  level: :info,
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
