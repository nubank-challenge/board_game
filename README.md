# NuBank Challenge Robots vs Dinosaurs

**Description**

This project was created for resolve a Nubank challenge Problem 1 - Robots vs Dinosaurs. This challenge consists in create an REST API to support those simulations.

These are the features required:

- Be able to create an empty simulation space - an empty 50 x 50 grid;
- Be able to create a robot in a certain position and facing direction;
- Be able to create a dinosaur in a certain position;
- Issue instructions to a robot - a robot can turn left, turn right, move forward, move backwards, and attack;
- A robot attack destroys dinosaurs around it (in front, to the left, to the right or behind);
- No need to worry about the dinosaurs - dinosaurs don't move;
- Display the simulation's current state;
- Two or more entities (robots or dinosaurs) cannot occupy the same position;
- Attempting to move a robot outside the simulation space is an invalid operation.

**Click on app link to see project details.*

**Setup**

You must have the docker and docker-compose installed, to run the project execute `docker-compose up` or `docker-compose up -d` if you want execute in background.

**API Documentation**

This proposed solution is detailed in the API documentation, that was written using OpenAPI 3.0 Specification, to access just click [API Documentation](http://localhost:8080 "API Documentation") or open http://localhost:8080 in your preferred browser.

**Tests**

Run `mix test` on the terminal to run the  tests for this project.
```bash
$ mix test
........................................................................................................................................

Finished in 8.1 seconds
136 tests, 0 failures

Randomized with seed 779466
```