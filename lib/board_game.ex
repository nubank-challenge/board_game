defmodule BoardGame do
  use GenServer

  alias BoardGame.Board

  def init({:ok, %Board{}} = state), do:
    state

  def start_link(game_id), do:
    GenServer.start_link(__MODULE__, Board.new(), name: game_id)

  def create do
    game_id = :"#{UUID.uuid1()}"
    {:ok, _pid} = DynamicSupervisor.start_child(BoardGame.DynamicSupervisor, {BoardGame, game_id})
    {:ok, game_id}
  end

  def destroy(game_id) do
    with {:ok, pid} <- running?(game_id) do
      BoardGame.DynamicSupervisor.terminate_child(pid)
    else
      error -> error
    end
  end

  def position_dinosaur(game_id, row, col) do
    validate_and_call(game_id, fn ->
      GenServer.call(game_id, {:position_dinosaur, row, col}, :infinity)
    end)
  end

  def position_robot(game_id, row, col, face_direction) do
    validate_and_call(game_id, fn ->
      GenServer.call(game_id, {:position_robot, row, col, face_direction}, :infinity)
    end)
  end

  def move_robot(game_id, row, col, movement) do
    validate_and_call(game_id, fn ->
      GenServer.call(game_id, {:move_robot, row, col, movement}, :infinity)
    end)
  end

  def turn_robot(game_id, row, col, diretion) do
    validate_and_call(game_id, fn ->
      GenServer.call(game_id, {:turn_robot, row, col, diretion}, :infinity)
    end)
  end

  def robot_attack(game_id, row, col) do
    validate_and_call(game_id, fn ->
      GenServer.call(game_id, {:robot_attack, row, col}, :infinity)
    end)
  end

  def robot_instructions(game_id, row, col) do
    validate_and_call(game_id, fn ->
      GenServer.call(game_id, {:robot_instructions, row, col}, :infinity)
    end)
  end

  def current_state(game_id) do
    validate_and_call(game_id, fn ->
      GenServer.call(game_id, :current_state, :infinity)
    end)
  end

  def handle_call({:position_dinosaur, row, col}, _from, board) do
    with {:ok, dinosaur, board} <- Board.position_dinosaur(board, row, col) do
      {:reply, {:ok, dinosaur}, board}
    else
      error -> {:reply, error, board}
    end
  end

  def handle_call({:position_robot, row, col, face_direction}, _from, board) do
    with {:ok, robot, board} <- Board.position_robot(board, row, col, face_direction) do
      {:reply, {:ok, robot}, board}
    else
      error -> {:reply, error, board}
    end
  end

  def handle_call({:move_robot, row, col, movement}, _from, board) do
    with {:ok, robot, board} <- Board.move_robot(board, row, col, movement) do
      {:reply, {:ok, robot}, board}
    else
      error -> {:reply, error, board}
    end
  end

  def handle_call({:turn_robot, row, col, diretion}, _from, board) do
    with {:ok, robot, board} <- Board.turn_robot(board, row, col, diretion) do
      {:reply, {:ok, robot}, board}
    else
      error -> {:reply, error, board}
    end
  end

  def handle_call({:robot_attack, row, col}, _from, board) do
    with {:ok, current_state, board} <- Board.robot_attack(board, row, col) do
      {:reply, {:ok, current_state}, board}
    else
      error -> {:reply, error, board}
    end
  end

  def handle_call({:robot_instructions, row, col}, _from, board) do
    with {:ok, instructions} <- Board.robot_instructions(board, row, col) do
      {:reply, {:ok, instructions}, board}
    else
      error -> {:reply, error, board}
    end
  end

  def handle_call(:current_state, _from, board) do
    with {:ok, current_state} <- Board.current_state(board) do
      {:reply, {:ok, current_state}, board}
    else
      error -> {:reply, error, board}
    end
  end

  defp validate_and_call(game_id, call) do
    with {:ok, _pid} <- running?(game_id) do
      call.()
    else
      error -> error
    end
  end

  defp running?(game_id) do
    case Process.whereis(game_id) do
      nil -> {:error, :not_found}
      pid -> {:ok, pid}
    end
  end
end
