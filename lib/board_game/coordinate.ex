defmodule BoardGame.Coordinate do
  alias __MODULE__

  @enforce_keys [:row, :col]
  defstruct [:row, :col]

  @board_range 1..50

  def new(row, col) when row in(@board_range) and col in(@board_range), do:
    {:ok, %Coordinate{row: row, col: col}}

  def new(_row, _col), do:
    {:error, :invalid_coordinate}

  def available?(coordinates, coordinate) do
    case Enum.member?(coordinates, coordinate) do
      true -> {:error, :overlapped_coordinate}
      _ -> :ok
    end
  end

end
