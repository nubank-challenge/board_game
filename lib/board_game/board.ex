defmodule BoardGame.Board do
  alias BoardGame.{Board, Coordinate, Dinosaur, Robot}

  defstruct robots: [], dinosaurs: []

  def new(), do:
    {:ok, %Board{}}

  def position_dinosaur(%Board{dinosaurs: dinosaurs} = board, row, col) do
    with {:ok, coordinate} <- Coordinate.new(row, col),
         :ok <- validate_coordinate(board, coordinate, :position),
         {:ok, dinosaur} <- Dinosaur.new(coordinate) do
      {:ok, dinosaur, %{board | dinosaurs: [dinosaur | dinosaurs]}}
    else
      error -> error
    end
  end

  def position_robot(%Board{robots: robots} = board, row, col, face_direction) do
    with {:ok, coordinate} <- Coordinate.new(row, col),
         :ok <- validate_coordinate(board, coordinate, :position),
         {:ok, robot} <- Robot.new(coordinate, face_direction) do
      {:ok, robot, %{board | robots: [robot | robots]}}
    else
      error -> error
    end
  end

  def move_robot(%Board{robots: robots} = board, row, col, movement) do
    with {:ok, coordinate} <- Coordinate.new(row, col),
         {:ok, robot, robots} <- Robot.find_and_extract(robots, coordinate),
         {:ok, robot} <- Robot.move(robot, movement, coordinates(board, :move)) do
      {:ok, robot, %{board | robots: [robot | robots]}}
    else
      error -> error
    end
  end

  def turn_robot(%Board{robots: robots} = board, row, col, diretion) do
    with {:ok, coordinate} <- Coordinate.new(row, col),
         {:ok, robot, robots} <- Robot.find_and_extract(robots, coordinate),
         {:ok, robot} <- Robot.turn(robot, diretion) do
      {:ok, robot, %{board | robots: [robot | robots]}}
    else
      error -> error
    end
  end

  def robot_attack(%Board{robots: robots, dinosaurs: dinosaurs} = board, row, col) do
    with {:ok, coordinate} <- Coordinate.new(row, col),
         {:ok, robot} <- Robot.find(robots, coordinate),
         {:ok, coordinates} <- Robot.coordinates_around(robot),
         {:ok, dinosaurs} <- Dinosaur.under_attack(dinosaurs, coordinates),
         board <- %{board | dinosaurs: dinosaurs},
         {:ok, current_state} <- current_state(board) do
      {:ok, current_state, board}
    else
      error -> error
    end
  end

  def robot_instructions(%Board{robots: robots} = board, row, col) do
    with {:ok, coordinate} <- Coordinate.new(row, col),
         {:ok, robot} <- Robot.find(robots, coordinate) do
      actions =
        robot
        |> Robot.possible_moves(coordinates(board, :move))
        |> Enum.map(&(:"move_#{&1}"))
        |> Enum.concat([:turn_left, :turn_right, :attack])

      {:ok, %{coordinate: coordinate, possible_actions: actions, face_direction: robot.face_direction}}
    else
      error -> error
    end
  end

  def current_state(%Board{robots: robots, dinosaurs: dinosaurs}) do
    dead_dinosaurs = Dinosaur.filter_by_state(dinosaurs, :dead)
    alive_dinosaurs = (dinosaurs -- dead_dinosaurs)

    {:ok, %{
      robots: robots,
      alive_dinosaurs: alive_dinosaurs,
      dead_dinosaurs: dead_dinosaurs
    }}
  end

  defp validate_coordinate(%Board{} = board, coordinate, action) do
    board
    |> coordinates(action)
    |> Coordinate.available?(coordinate)
  end

  defp coordinates(%Board{robots: robots, dinosaurs: dinosaurs}, :position) do
    dinosaurs
    |> Enum.concat(robots)
    |> Enum.map(&(&1.coordinate))
  end

  defp coordinates(%Board{robots: robots, dinosaurs: dinosaurs}, :move) do
    dinosaurs
    |> Dinosaur.filter_by_state(:alive)
    |> Enum.concat(robots)
    |> Enum.map(&(&1.coordinate))
  end

end
