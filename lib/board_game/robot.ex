defmodule BoardGame.Robot do
  alias BoardGame.{Coordinate, Robot}

  @enforce_keys [:coordinate, :face_direction]
  defstruct [:coordinate, :face_direction]

  @directions [:up, :down, :left, :right]
  @directions_values %{up: [1, 0], down: [-1, 0], left: [0, -1], right: [0, 1]}
  @moves [:forward, :backwards]

  def new(%Coordinate{} = coordinate, face_direction) when face_direction in(@directions), do:
    {:ok, %Robot{coordinate: coordinate, face_direction: face_direction}}

  def new(%Coordinate{}, _face_direction), do:
    {:error, :invalid_face_direction}

  def move(%Robot{coordinate: %Coordinate{row: row, col: col}, face_direction: :up} = robot, :forward, coordinates), do:
    validate_and_move(robot, row + 1, col, coordinates)

  def move(%Robot{coordinate: %Coordinate{row: row, col: col}, face_direction: :down} = robot, :forward, coordinates), do:
    validate_and_move(robot, row - 1, col, coordinates)

  def move(%Robot{coordinate: %Coordinate{row: row, col: col}, face_direction: :left} = robot, :forward, coordinates), do:
    validate_and_move(robot, row, col - 1, coordinates)

  def move(%Robot{coordinate: %Coordinate{row: row, col: col}, face_direction: :right} = robot, :forward, coordinates), do:
    validate_and_move(robot, row, col + 1, coordinates)

  def move(%Robot{coordinate: %Coordinate{row: row, col: col}, face_direction: :up} = robot, :backwards, coordinates), do:
    validate_and_move(robot, row - 1, col, coordinates)

  def move(%Robot{coordinate: %Coordinate{row: row, col: col}, face_direction: :down} = robot, :backwards, coordinates), do:
    validate_and_move(robot, row + 1, col, coordinates)

  def move(%Robot{coordinate: %Coordinate{row: row, col: col}, face_direction: :left} = robot, :backwards, coordinates), do:
    validate_and_move(robot, row, col + 1, coordinates)

  def move(%Robot{coordinate: %Coordinate{row: row, col: col}, face_direction: :right} = robot, :backwards, coordinates), do:
    validate_and_move(robot, row, col - 1, coordinates)

  def move(%Robot{}, _movement, _coordinates), do:
    {:error, :invalid_movement}

  def turn(%Robot{face_direction: :up} = robot, :left), do:
    {:ok, %{robot | face_direction: :left}}

  def turn(%Robot{face_direction: :down} = robot, :left), do:
    {:ok, %{robot | face_direction: :right}}

  def turn(%Robot{face_direction: :left} = robot, :left), do:
    {:ok, %{robot | face_direction: :down}}

  def turn(%Robot{face_direction: :right} = robot, :left), do:
    {:ok, %{robot | face_direction: :up}}

  def turn(%Robot{face_direction: :up} = robot, :right), do:
    {:ok, %{robot | face_direction: :right}}

  def turn(%Robot{face_direction: :down} = robot, :right), do:
    {:ok, %{robot | face_direction: :left}}

  def turn(%Robot{face_direction: :left} = robot, :right), do:
    {:ok, %{robot | face_direction: :up}}

  def turn(%Robot{face_direction: :right} = robot, :right), do:
    {:ok, %{robot | face_direction: :down}}

  def turn(%Robot{}, _diretion), do:
    {:error, :invalid_direction}

  def possible_moves(%Robot{} = robot, coordinates), do:
    Enum.filter(@moves, &(match?({:ok, _}, Robot.move(robot, &1, coordinates))))

  def coordinates_around(%Robot{coordinate: %Coordinate{row: row, col: col}}) do
    coordinates =
      @directions_values
      |> Enum.map(fn {_, [add_to_row, add_to_col]} -> Coordinate.new(row + add_to_row, col + add_to_col) end)
      |> Enum.filter(&(match?({:ok, _}, &1)))
      |> Enum.map(fn {_, coordinate} -> coordinate end)
    {:ok, coordinates}
  end

  def find(robots, %Coordinate{row: row, col: col}) do
    with %Robot{} = robot <- Enum.find(robots, &(match?(%Coordinate{row: ^row, col: ^col}, &1.coordinate))) do
      {:ok, robot}
    else
      nil -> {:error, :robot_not_found}
    end
  end

  def find_and_extract(robots, coordinate) do
    with {:ok, robot} <- Robot.find(robots, coordinate) do
      robots = List.delete(robots, robot)
      {:ok, robot, robots}
    else
      error -> error
    end
  end

  defp validate_and_move(robot, row, col, coordinates) do
    with {:ok, coordinate} <- Coordinate.new(row, col),
         :ok <- Coordinate.available?(coordinates, coordinate) do
      {:ok, %{robot | coordinate: coordinate}}
    else
      {:error, :invalid_coordinate} -> {:error, :move_out_of_board}
      error -> error
    end
  end

end
