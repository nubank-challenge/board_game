defmodule BoardGame.Dinosaur do
  alias BoardGame.{Coordinate, Dinosaur}

  @enforce_keys [:coordinate, :state]
  defstruct [:coordinate, :state]

  def new(%Coordinate{} = coordinate), do:
    {:ok, %Dinosaur{coordinate: coordinate, state: :alive}}

  def filter_by_state(dinosaurs, state), do:
    Enum.filter(dinosaurs, &(&1.state == state))

  def under_attack(dinosaurs, coordinates), do:
    {:ok, Enum.reduce(coordinates, dinosaurs, &state_update/2)}

  def find(dinosaurs, row, col) do
    with %Dinosaur{} = dinosaur <- Enum.find(dinosaurs, &(match?(%Coordinate{row: ^row, col: ^col}, &1.coordinate))) do
      {:ok, dinosaur}
    else
      nil -> {:error, :dinosaur_not_found}
    end
  end

  def find_and_extract(dinosaurs, row, col) do
    with {:ok, dinosaur} <- Dinosaur.find(dinosaurs, row, col) do
      dinosaurs = List.delete(dinosaurs, dinosaur)
      {:ok, dinosaur, dinosaurs}
    else
      error -> error
    end
  end

  defp state_update(%Coordinate{row: row, col: col}, dinosaurs) do
    case Dinosaur.find_and_extract(dinosaurs, row, col) do
      {:ok, dinosaur, dinosaurs} -> [%{dinosaur | state: :dead} | dinosaurs]
      _ -> dinosaurs
    end
  end

end
