defmodule BoardGameWeb.Api.V1.HealthController do
  use BoardGameWeb, :controller

  def health(conn, _params) do
    with %{workers: games_count} <- BoardGame.DynamicSupervisor.count_children() do
      json(conn, %{success: true, games: games_count})
    else
      error ->
        conn
        |> put_status(:internal_server_error)
        |> json(%{success: false, error: inspect(error)})
    end
  end
end
