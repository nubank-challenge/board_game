defmodule BoardGameWeb.Api.V1.GameController do
  use BoardGameWeb, :controller

  def create(conn, _params) do
    with {:ok, game_id} <- BoardGame.create() do
      conn
      |> put_status(:created)
      |> render("create.json", game_id: game_id)
    else
      error -> render_error(conn, error)
    end
  end

  def delete(conn, %{"id" => game_id}) do
    with :ok <- BoardGame.destroy(:"#{game_id}") do
      conn
      |> put_resp_header("content-type", "application/json")
      |> send_resp(:no_content, "")
    else
      error -> render_error(conn, error)
    end
  end

  def show(conn, %{"id" => game_id}) do
    with {:ok, current_state} <- BoardGame.current_state(:"#{game_id}") do
      render(conn, "current_state.json", current_state: current_state)
    else
      error -> render_error(conn, error)
    end
  end

  def position_dinosaur(conn, %{"game_id" => game_id, "coordinate" => %{"row" => row, "col" => col}}) do
    with {:ok, dinosaur} <- BoardGame.position_dinosaur(:"#{game_id}", row, col) do
      conn
      |> put_status(:created)
      |> render("dinosaur.json", dinosaur: dinosaur)
    else
      error -> render_error(conn, error)
    end
  end

  def position_dinosaur(conn, _params), do:
    render_error(conn, {:error, :missing_coordinate})

  def position_robot(conn, %{"game_id" => game_id, "face_direction" => face_direction, "coordinate" => %{"row" => row, "col" => col}}) do
    with {:ok, robot} <- BoardGame.position_robot(:"#{game_id}", row, col, :"#{face_direction}") do
      conn
      |> put_status(:created)
      |> render("robot.json", robot: robot)
    else
      error -> render_error(conn, error)
    end
  end

  def position_robot(conn, %{"face_direction" => _face_direction}), do:
    render_error(conn, {:error, :missing_coordinate})

  def position_robot(conn, _params), do:
    render_error(conn, {:error, :missing_face_direction})

  def robot_action(conn, %{"game_id" => game_id, "action" => "move_" <> movement, "coordinate" => %{"row" => row, "col" => col}}) do
    with {:ok, robot} <- BoardGame.move_robot(:"#{game_id}", row, col, :"#{movement}") do
      render(conn, "robot.json", robot: robot)
    else
      error -> render_error(conn, error)
    end
  end

  def robot_action(conn, %{"game_id" => game_id, "action" => "turn_" <> diretion, "coordinate" => %{"row" => row, "col" => col}}) do
    with {:ok, robot} <- BoardGame.turn_robot(:"#{game_id}", row, col, :"#{diretion}") do
      render(conn, "robot.json", robot: robot)
    else
      error -> render_error(conn, error)
    end
  end

  def robot_action(conn, %{"game_id" => game_id, "action" => "attack", "coordinate" => %{"row" => row, "col" => col}}) do
    with {:ok, current_state} <- BoardGame.robot_attack(:"#{game_id}", row, col) do
      render(conn, "current_state.json", current_state: current_state)
    else
      error -> render_error(conn, error)
    end
  end

  def robot_action(conn, %{"action" => _action, "coordinate" => _coordinate}), do:
    render_error(conn, {:error, :invalid_robot_action})

  def robot_action(conn, %{"action" => _action}), do:
    render_error(conn, {:error, :missing_coordinate})

  def robot_action(conn, _params), do:
    render_error(conn, {:error, :missing_robot_action})

  def robot_instructions(conn, %{"game_id" => game_id, "coordinate" => %{"row" => row, "col" => col}}) do
    with {:ok, instructions} <- BoardGame.robot_instructions(:"#{game_id}", row, col) do
      render(conn, "robot_instructions.json", instructions: instructions)
    else
      error -> render_error(conn, error)
    end
  end

  def robot_instructions(conn, _params), do:
    render_error(conn, {:error, :missing_coordinate})

  @not_found_errors [:not_found, :robot_not_found]
  defp render_error(conn, {:error, error}) when error in(@not_found_errors) do
    conn
    |> put_status(:not_found)
    |> render("error.json", error: error)
  end

  @bad_request_errors [:missing_coordinate, :missing_face_direction, :missing_robot_action]
  defp render_error(conn, {:error, error}) when error in(@bad_request_errors) do
    conn
    |> put_status(:bad_request)
    |> render("error.json", error: error)
  end

  @unprocessable_entity_errors [
    :invalid_coordinate, :invalid_face_direction, :invalid_robot_action,
    :invalid_movement, :invalid_direction, :overlapped_coordinate, :move_out_of_board
  ]
  defp render_error(conn, {:error, error}) when error in(@unprocessable_entity_errors) do
    conn
    |> put_status(:unprocessable_entity)
    |> render("error.json", error: error)
  end

  defp render_error(conn, _error) do
    conn
    |> put_status(:internal_server_error)
    |> render("error.json", error: :internal_server_error)
  end
end
