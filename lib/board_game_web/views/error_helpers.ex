defmodule BoardGameWeb.ErrorHelpers do
  @moduledoc """
  Conveniences for translating and building error messages.
  """

  @doc """
  Translates an error message using gettext.
  """
  def translate_error(:not_found), do: "Resource not found"
  def translate_error(:robot_not_found), do: "Robot not found"
  def translate_error(:missing_coordinate), do: "Coordinate required"
  def translate_error(:missing_face_direction), do: "Face direction required"
  def translate_error(:missing_robot_action), do: "Robot action required"
  def translate_error(:invalid_coordinate), do: "Invalid coordinate"
  def translate_error(:invalid_face_direction), do: "Invalid face direction"
  def translate_error(:invalid_robot_action), do: "Invalid robot action"
  def translate_error(:invalid_movement), do: "Invalid movement"
  def translate_error(:invalid_direction), do: "Invalid direction"
  def translate_error(:overlapped_coordinate), do: "Overlapped coordinate"
  def translate_error(:move_out_of_board), do: "Move out of board"
  def translate_error(:internal_server_error), do: "Internal Server Error"
  def translate_error(error) when is_bitstring(error), do: String.capitalize(error)
  def translate_error(error) when is_atom(error) do
    error
    |> Atom.to_string()
    |> String.replace("_", " ")
    |> String.capitalize()
  end

  def translate_error({msg, opts}) do
    # When using gettext, we typically pass the strings we want
    # to translate as a static argument:
    #
    #     # Translate "is invalid" in the "errors" domain
    #     dgettext("errors", "is invalid")
    #
    #     # Translate the number of files with plural rules
    #     dngettext("errors", "1 file", "%{count} files", count)
    #
    # Because the error messages we show in our forms and APIs
    # are defined inside Ecto, we need to translate them dynamically.
    # This requires us to call the Gettext module passing our gettext
    # backend as first argument.
    #
    # Note we use the "errors" domain, which means translations
    # should be written to the errors.po file. The :count option is
    # set by Ecto and indicates we should also apply plural rules.
    if count = opts[:count] do
      Gettext.dngettext(BoardGameWeb.Gettext, "errors", msg, msg, count, opts)
    else
      Gettext.dgettext(BoardGameWeb.Gettext, "errors", msg, opts)
    end
  end
end
