defmodule BoardGameWeb.Api.V1.GameView do
  use BoardGameWeb, :view

  def render("create.json", %{game_id: game_id}), do:
    %{game_id: game_id}

  def render("current_state.json", %{current_state: %{alive_dinosaurs: alive_dinosaurs, dead_dinosaurs: dead_dinosaurs, robots: robots}}) do
    %{
      dinosaurs: %{
        alive: render_many(alive_dinosaurs, __MODULE__, "dinosaur", as: :dinosaur),
        dead: render_many(dead_dinosaurs, __MODULE__, "dinosaur", as: :dinosaur)
      },
      robots: render_many(robots, __MODULE__, "robot", as: :robot)
    }
  end

  def render("dinosaur.json", %{dinosaur: dinosaur}), do:
    %{dinosaur: render("dinosaur", dinosaur: dinosaur)}

  def render("dinosaur", %{dinosaur: %{coordinate: %{row: row, col: col}}}), do:
    %{coordinate: %{row: row, col: col}}

  def render("robot.json", %{robot: robot}), do:
    %{robot: render("robot", robot: robot)}

  def render("robot", %{robot: %{coordinate: %{row: row, col: col}, face_direction: face_direction}}), do:
    %{coordinate: %{row: row, col: col}, face_direction: face_direction}

  def render("robot_instructions.json", %{instructions:
    %{coordinate: %{row: row, col: col}, possible_actions: possible_actions, face_direction: face_direction}}) do
    %{
      coordinate: %{row: row, col: col},
      face_direction: face_direction,
      possible_actions: possible_actions
    }
  end

  def render("error.json", %{error: error}), do:
    %{code: error, error: translate_error(error)}

end
