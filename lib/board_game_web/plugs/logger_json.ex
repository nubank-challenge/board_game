defmodule BoardGameWeb.Plugs.LoggerJSON do
  def extra_attributes(conn) do
    %{
      response_body: to_string(conn.resp_body),
      request_headers: conn.req_headers |> Enum.into(%{})
    }
  end
end
