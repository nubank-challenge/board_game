defmodule BoardGameWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :board_game

  socket "/socket", BoardGameWeb.UserSocket,
    websocket: true,
    longpoll: false

  plug CORSPlug

  plug Plug.LoggerJSON,
    log: Logger.level,
    include_debug_logging: true,
    extra_attributes_fn: &BoardGameWeb.Plugs.LoggerJSON.extra_attributes/1

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phx.digest
  # when deploying your static files in production.
  plug Plug.Static,
    at: "/",
    from: :board_game,
    gzip: false,
    only: ~w(css fonts images js favicon.ico robots.txt)

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    plug Phoenix.CodeReloader
  end

  plug Plug.RequestId
  plug Plug.Telemetry, event_prefix: [:phoenix, :endpoint]

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug Plug.MethodOverride
  plug Plug.Head

  # The session will be stored in the cookie and signed,
  # this means its contents can be read but not tampered with.
  # Set :encryption_salt if you would also like to encrypt it.
  plug Plug.Session,
    store: :cookie,
    key: "_board_game_key",
    signing_salt: "3j6nGF0D"

  plug BoardGameWeb.Router
end
