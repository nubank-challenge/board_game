defmodule BoardGameWeb.Router do
  require Logger

  use BoardGameWeb, :router
  use Plug.ErrorHandler

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", BoardGameWeb.Api, as: :api do
    pipe_through :api

    scope "/v1", V1, as: :v1 do
      get "/health", HealthController, :health
      resources "/games", GameController, only: [:create, :delete, :show] do
        post "/dinosaurs", GameController, :position_dinosaur

        scope "/robots" do
          post "/", GameController, :position_robot
          put "/", GameController, :robot_action
          post "/instructions", GameController, :robot_instructions
        end
      end
    end
  end

  defp handle_errors(%Plug.Conn{status: 500} = conn, %{kind: kind, reason: reason, stack: stacktrace}) do
    Plug.LoggerJSON.log_error(kind, reason, stacktrace)
    send_resp(conn, 500, Poison.encode!(%{errors: %{detail: "Internal server error"}}))
  end

  defp handle_errors(_, _), do: nil
end
